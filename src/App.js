import React, { Suspense } from 'react';
import { useRoutes } from "react-router-dom";
import { useSelector } from "react-redux";

import BackdropLoad from './components/BackdropLoad'
import { Public } from './routes/public'
import { Private } from './routes/private'

const App = () => {

  const publicRoutes = useRoutes(Public);
  const priveteRoutes = useRoutes(Private);
  const auth = useSelector((state) => state.user.user);
  
  return (
    <Suspense fallback={<BackdropLoad />}>
      { auth ? priveteRoutes : publicRoutes }
    </Suspense>
  )
}

export default App;