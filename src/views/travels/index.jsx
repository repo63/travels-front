import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { Box, IconButton } from "@mui/material";
import { useSelector } from "react-redux";
import AddRoundedIcon from '@mui/icons-material/AddRounded';

import Page from '../../components/Page';
import BackdropLoad from '../../components/BackdropLoad';
import Table from '../../components/dataTable';

const Api = 'http://localhost:3001'

const Index = () => {

    const [loading, setLoading] = useState(false)
    const [viajes, setViajes] = useState([]);

    const user = useSelector((state) => state.user.user)

    useEffect(() => {
        const getViajes = async () => {
            setLoading((state) => !state);
            try {
                const viaje = await fetch(`${Api}/viajes`);
                const data_viajes = await viaje.json();

                const usuario = await fetch(`${Api}/usuarios`);
                const data_usuarios = await usuario.json();

                const current_User = data_usuarios.data.filter((item) => item.email == user.email);

                if (current_User.filter((item) => item.tipoUsuario === 'Administrador').length > 0) {
                    setViajes(data_viajes.data);
                } else {
                    setViajes(data_viajes.data.filter(item => item.usuarioEmail === user.email));
                }
            } catch (err) {
                console.log(err);
            } finally {
                setLoading((state) => !state);
            }
        };
        getViajes();
    }, [user])

    return (
        <Page title={'Viajes'} subtitle={''}>
            <BackdropLoad open={loading} />
            <Box sx={{ display: "flex", justifyContent: "flex-end", paddingBottom: '5%', }} >
                <IconButton
                    sx={{
                        color: 'white',
                        background: '#136a8a',
                        '&:hover': {
                            background: '#136a8a',
                            transform: 'scale(1.20)'

                        },
                    }}
                    component={NavLink} to={'/travels/create'}
                >
                    <AddRoundedIcon sx={{ fontSize: 40 }} />
                </IconButton>
            </Box>
            {viajes.length > 0 && <Table rows={viajes} />}
        </Page>
    )
};

export default Index;