import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { Paper, CardContent, Grid, TextField, Autocomplete, Button } from '@mui/material';
import { useSelector } from 'react-redux';

import Page from '../../components/Page';
import BackdropLoad from '../../components/BackdropLoad';

const Api = 'http://localhost:3001'
const paperStyle = { height: '80%', width: '90%', margin: "40px auto", paddingTop: '3%' }

const Create = () => {

    const navigate = useNavigate();
    const user = useSelector((state) => state.user.user)

    const [loading, setLoading] = useState(false)
    const [viaje, setViaje] = useState({
        puntoInicio: '',
        puntoTermino: '',
        km: '',
        transporte: '',
        emision: '',
        personas: '',
        idaVuelta: '',
        usuarioEmail: user.email
    });
    const [transportes, setTransportes] = useState([]);
    const [trabajadores, setTrabajadores] = useState([]);

    useEffect(() => {
        const getApis = async () => {
            setLoading((state) => !state);
            try {
                const transporte = await fetch(`${Api}/transportes`);
                const trabajadores = await fetch(`${Api}/trabajadores`);
                const data_transporte = await transporte.json();
                const data_trabajadores = await trabajadores.json();
                setTransportes(data_transporte.data);
                setTrabajadores(data_trabajadores.data);
            } catch (err) {
                console.log(err);
            } finally {
                setLoading((state) => !state);
            }
        };
        getApis();
    }, [])

    const change = (e) => {
        const { name, value } = e.target;
        setViaje((data) => ({ ...data, [name]: value }))
    }

    const onSubmit = async (e) => {
        e.preventDefault();
        setLoading((state) => !state);
        const res = await fetch(`${Api}/viajes`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(viaje),
        });
        if (res.status === 200) {
            navigate('/travels')
        }
        setLoading((state) => !state);
    };

    return (
        <Page title={'Nuevo Viaje'} subtitle={'Viajes'} goback={true}>
            <BackdropLoad open={loading} />
            <Paper elevation={10} style={paperStyle} >
                <CardContent >
                    <form onSubmit={onSubmit}>
                        <Grid container spacing={4}>
                            <Grid item xs={12} sm={6} md={6} >
                                <TextField
                                    fullWidth
                                    name={'puntoInicio'}
                                    label={'Dirección del punto de partida. (*)'}
                                    value={viaje.puntoInicio}
                                    onChange={change}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} >
                                <TextField
                                    fullWidth
                                    name={'puntoTermino'}
                                    label={'Dirección del punto de término. (*)'}
                                    value={viaje.puntoTermino}
                                    onChange={change}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} >
                                <Autocomplete
                                    options={transportes}
                                    getOptionLabel={(option) => option.nombre}
                                    onChange={(_, target) =>
                                        setViaje((data) => ({
                                            ...data,
                                            transporte: target.nombre,
                                            emision: target.emision
                                        }))
                                    }
                                    disableClearable
                                    filterSelectedOptions
                                    renderInput={(params) => (
                                        <TextField {...params} label='Medio de transporte. (*)' />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} >
                                <TextField
                                    fullWidth
                                    type={'number'}
                                    name={'km'}
                                    label={'Kilometros recorridos. (*)'}
                                    value={viaje.km}
                                    onChange={change}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} >
                                <Autocomplete
                                    multiple
                                    options={trabajadores}
                                    getOptionLabel={(option) => `${option.nombre} ${option.apellido || ''}`}
                                    disableCloseOnSelect
                                    disableClearable
                                    filterSelectedOptions
                                    defaultValue={[]}
                                    onChange={(_, target) =>
                                        setViaje((data) => ({
                                            ...data,
                                            personas: target.map((item) => item._id),
                                        }))
                                    }
                                    renderInput={(params) => (
                                        <TextField {...params} label="Trabajadores. (*)" />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} md={6} >
                                <Autocomplete
                                    options={[
                                        { label: 'Solo ida', value: 0 },
                                        { label: 'Ida y vuelta', value: 1 },
                                    ]}
                                    getOptionLabel={(option) => option.label}
                                    onChange={(_, target) =>
                                        setViaje((data) => ({
                                            ...data,
                                            idaVuelta: target.value,
                                        }))
                                    }
                                    disableClearable
                                    filterSelectedOptions
                                    renderInput={(params) => (
                                        <TextField {...params} label='Viaje. (*)' />
                                    )}
                                />
                            </Grid>
                            {
                                (viaje.puntoInicio &&
                                    viaje.puntoTermino &&
                                    viaje.transporte &&
                                    viaje.km &&
                                    viaje.personas.length > 0 &&
                                    viaje.idaVuelta !== '') &&

                                <Grid item xs={12} >
                                    <Grid container spacing={2} justifyContent="flex-end">
                                        <Grid item xs={6} sm={4}>
                                            <Button
                                                fullWidth
                                                variant="outline"
                                                type="submit"
                                                sx={{
                                                    textTransform: "none",
                                                    color: 'white',
                                                    background: '#136a8a',
                                                    '&:hover': {
                                                        background: '#136a8a',
                                                        transform: 'scale(1.01)'
                                                    },
                                                }}
                                            >
                                                Guadar
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            }
                        </Grid>
                    </form>
                </CardContent>
            </Paper>
        </Page>
    );
};

export default Create;