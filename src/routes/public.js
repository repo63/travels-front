import { Navigate } from 'react-router-dom'

import SignIn from '../views/auth/SignIn'

export const Public = [
    { path: '/', element: <Navigate to={'login'} /> },
    { path: '*', element: <Navigate to={'login'} /> },
    { path: '/login', element: <SignIn /> }
]