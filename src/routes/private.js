import { lazy } from 'react';
import { Navigate } from 'react-router-dom';

const Layout = lazy(() => import('../components/layauts'));

const Travels = lazy(() => import('../views/travels'));
const TravelsCreate = lazy(() => import('../views/travels/create'));

export const Private = [
    {
        element: <Layout />,
        children: [
            { path: '/', element: <Navigate to="/travels" /> },
            // { path: '*', element: <Error code="404" /> },
            { path: 'login', element: <Navigate to="/travels" /> },

            { path: 'travels', element: <Travels /> },
            { path: 'travels/create', element: <TravelsCreate /> }
        ]
    }
];