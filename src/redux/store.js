import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./reducer"

export const store = configureStore ({
    reducer: {
        user: userReducer
    }
})

// const authSlice = createSlice ({
//     name: 'auth',
//     initialState: {
//         auth: false
//     },
//     reducers: {
//         getSignIn: (state) => {
//             state.auth = !state.auth;
//         }
//     }
// })

// const reducer = authSlice.reducer;
// export const store = configureStore ({ reducer })
// export const { getSignIn } = authSlice.actions;



