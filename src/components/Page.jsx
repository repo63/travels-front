import React, { forwardRef } from "react";
import { Helmet } from "react-helmet";
import { Container } from "@mui/material";

import ToolBar from './ToolBar';

const Page = forwardRef(({ children, title = "Title", subtitle = "Subtitle", goback, ...rest }, ref) => {

  return (
    <div
      ref={ref}
      {...rest}
      style={{
        paddingTop: 50,
        paddingBottom: 30,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <Container maxWidth={'lg'}>
        <ToolBar title={title} subtitle={subtitle} goback={goback}/>
        {children}
      </Container>
    </div>
  );
}
);
export default Page;
