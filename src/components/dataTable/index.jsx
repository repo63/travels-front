import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#136a8a',
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

export default function CustomizedTables({ rows, data }) {

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 800 }} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell align="center">Punto de Inicio</StyledTableCell>
                        <StyledTableCell align="center">Punto de Termino</StyledTableCell>
                        <StyledTableCell align="center">Km Recorridos</StyledTableCell>
                        <StyledTableCell align="center">Medio de Transporte</StyledTableCell>
                        <StyledTableCell align="center">Numero de Personas</StyledTableCell>
                        <StyledTableCell align="center">Ida y Vuelta</StyledTableCell>
                        <StyledTableCell align="center">KgCO2</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <StyledTableRow key={row.name}>
                            <StyledTableCell align="center">{row.puntoInicio}</StyledTableCell>
                            <StyledTableCell align="center">{row.puntoTermino}</StyledTableCell>
                            <StyledTableCell align="center">{row.km}</StyledTableCell>
                            <StyledTableCell align="center">{row.transporte}</StyledTableCell>
                            <StyledTableCell align="center">{row.personas.length}</StyledTableCell>
                            <StyledTableCell align="center">{row.idaVuelta ? 'Verdadero' : 'Falso'}</StyledTableCell>
                            <StyledTableCell align="center">{`${((row.emision*row.km)*(row.idaVuelta ? 2 : 1)).toFixed(2)}`}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}