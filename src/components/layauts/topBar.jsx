import React, { memo } from 'react';
import { AppBar, Box, IconButton, Toolbar } from "@mui/material";
import { useDispatch } from 'react-redux';
import { logout } from '../../redux/reducer';
import LoginOutlinedIcon from '@mui/icons-material/LoginOutlined';

const TopBar = () => {

    const dispatch = useDispatch();

    return (
        <AppBar
            elevation={5}
            sx={{
                background: '#2C5364'
            }}
        >
            <Toolbar>
                <Box flexGrow={1} />
                <IconButton
                    color="inherit"
                    sx={{
                        '&:hover': {
                            background: '#2C3E50',
                            transform: 'scale(1.20)'
                        },
                    }}
                    onClick={() => dispatch(logout())}
                >
                    <LoginOutlinedIcon />
                </IconButton>
            </Toolbar>
        </AppBar>
    );
};

export default memo(TopBar);
