import { CardHeader, Typography, Button } from "@mui/material";
import { NavLink } from "react-router-dom";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

const ToolBar = ({ title, subtitle, goback = false }) => {
    return (
        <CardHeader
            title={
                goback ?
                    <Button
                        size={'large'}
                        variant={'text'}
                        sx={{
                            textTransform: "none",
                            color: 'black',
                            '&:hover': {
                                transform: 'scale(1.09)',
                            }
                        }}
                        startIcon={<ArrowBackIosIcon />}
                        component={NavLink} to={'/travels'}
                    >
                        {subtitle}
                    </Button>
                    :
                    <Typography variant={'h5'} >
                        {subtitle}
                    </Typography>
            }
            subheader={
                <Typography variant={'h6'}  >
                    {title}
                </Typography>
            }
        />
    );
};

export default ToolBar;